/* Copyright ALexander Tkachenko , aka ALT
*  ALTernative Mighty Solutions
*  http://alternative.ms
*/
using UnityEngine;
using System.Collections;

// класс SnakeGameScript наследник от класса MonoBehaviour , а он наследник от класса Behaviour , а он наследник от класса Component , а он наследник от класса Object , который есть базовый класс из UnityEngine :)

public class SnakeGameScript : MonoBehaviour { // вся игра в одном скрипте

	/*[System.Serializable] // этим мы получаем доступ к полям класса в инспекторе
	public class dummy2D // объявляем свой класс-пустышку для создания двумерного массива , который будет отображаться в редакторе Unity
	{
		public GameObject[] cellsX = new GameObject[12]; // это убдет у нас массив-строка из 12 клеточек игрового поля
	}

	public dummy2D[] screenBlocks = new dummy2D[12]; // обхявляем массив "строк" - и получаем двумерный массив
	*/
	[System.Serializable]
	public enum MoveDir // перечисление направлений движения , хотя я предпочитаю юзать обычный int , но так выглядет круче :) типа "я юзаю энумераторы , бла-бла-бла"...
	{
		Left,
		Right,
		Up,
		Down,
	};

	public MoveDir moveDir = MoveDir.Right; // объвявляем переменную и сразу инициализируем её задав первичное значение , что бы она не была null
	public MoveDir wantedMoveDir = MoveDir.Right;

	public GUITexture[] arrowButtons; // сюда мы положим наши кнопки стрелочек с экрана : 0 - Left , 1 - Right , 2 - Up , 3 - Down . Конечно, можно объявить 4 GUITexture и назначить каждую из них , но так типа круче :)

	public Transform GameBoard; // контейнер для клеточке игрвого поля

	public GameObject[] cellTypes; // сюда мы положим "префабы" наших клеточек , всех типов , в таком порядке : 0 - SnakeBody , 1 - PlusLength , 2 - MinusLength , 3 - Reverse , 4 - SpeedUp , 5 - SlowDown

	public GameObject firstSnakeCell = null;
	public GameObject lastSnakeCell = null;
	public GameObject dummyCell = null;
	public GameObject dummy2Cell = null;

	public GameObject[] snakeBodyBlocks = new GameObject[144]; // массив блоков на игровом поле
	public int snakeLength = 4;

	public bool gameStarted = false;

	public float stepTimer = 0.0f;
	public float maxStepTimer = 0.7f;

	public Vector2 firstCellPos = new Vector2(5,5);
	public Vector2 lastCellPos = new Vector2(2,5);

	private Vector2 dummyPos = new Vector2(2,5);

	public bool needFlipDir = false;

	private bool canMove = true;

	private int newX;
	private int newY;

	private bool needCreateReverseApple = false;

	public GameObject currentApple = null;

	public int appleStep = 0;

	private int score = 0;
	private string scoreString = "";

	//public GUIText scoreLabel;
	//public GUIText scoreLabelShadow;

	private float GUIMultipler = 1.0f;
	private float GUIMultiplerW = 1.0f;

	public Camera LinesCamera;

	//public GameObject ScreenLines;

	//public GUITexture ScreenGloss;

	public GUIStyle ALTernativeMSLabelStyle;
	public GUIStyle GameTitleLabelStyle;

	public GUIStyle ScreenGlossStyle;
	public GUIStyle ScreenLinesStyle;
	public GUIStyle ScreenLinesStyleX3;


	public GUIStyle ScoreStyle;

	//public GUIStyle LeftArrowButton;

	//public GUIStyle LegendStyle;

	void OnGUI() {
		// все вычисления для координат GUI нужно делать только в этой функции
		GUIMultipler = (Screen.height / 480.0f); //Mathf.Min( (Screen.height/480.0f),(Screen.width/854.0f) );
		GUIMultiplerW = (Screen.width / 854.0f);
		float SULX = ( (Screen.width-854.0f*GUIMultipler)/2.0f)/GUIMultipler;
		float SULY = ( (Screen.height-480.0f*GUIMultipler)/2.0f)/GUIMultipler;
		
		GUI.matrix = Matrix4x4.TRS(Vector3.zero,Quaternion.identity,new Vector3(GUIMultipler,GUIMultipler,1.0f)); // задаём нужный масштаб

		GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f); // в начале задаём что сейчас будем рисовать непрозрачное, на всякиц случай

		//ScreenGloss.pixelInset = new Rect (-Screen.width/2.0f, -Screen.height/2.0f, Screen.width, Screen.height);

		LinesCamera.orthographicSize = (int)(Screen.height / 2.0f);
		//camera.orthographicSize = (int)(Screen.height / 2);
		//GUI.color = new Color(1.0f, 1.0f, 1.0f, Alpha7BG);
		//GUI.Box( Rect( SULX, SULY, 1280, 1024 ) , "", Scene7_BG); // рисуем фон ep2.kadr8 - олени разбегаються по лесу - воет волк

		// рисуем элементы экрана
		GUI.Label( new Rect( (int)(SULX*2+0), (int)(0+0), 854, 480 ) , "ALTernative.MS" , ALTernativeMSLabelStyle); // левый нижний угол , при условии что текст lower right
		//GUI.Label( new Rect( /*(Screen.width-109*GUIMultipler), (Screen.height-21*GUIMultipler)*/ (int)(SULX+0), (int)(SULY+0), 854, 480 ) , "ALTernative.MS" , ALTernativeMSLabelStyle);
		GUI.Label( new Rect( (int)(SULX+0), (int)(0+3*1), 854, 480 ) , "SNAKE'84 v0.1 alpha" , GameTitleLabelStyle); // поцентру сверху , при условии что текст upper center
		GUI.Label( new Rect( (int)(SULX+32*1), (int)(0+37*1), 854, 480 ) , scoreString , ScoreStyle); // score
		GUI.Label( new Rect( (int)(SULX+649*1), (int)(0+37*1), 854, 480 ) , "Legend:" , ScoreStyle); // Legend:
		GUI.Label( new Rect( (int)(SULX+695*1), (int)(0+82*1), 854, 480 ) , "- Snake body" , ScoreStyle); // Snake body // всё очень точно и просто задаёться :)
		GUI.Label( new Rect( (int)(SULX+695*1), (int)(0+138*1), 854, 480 ) , "- Length +1" , ScoreStyle); // Length +1
		GUI.Label( new Rect( (int)(SULX+695*1), (int)(0+190*1), 854, 480 ) , "- Length -1" , ScoreStyle); // Length -1
		GUI.Label( new Rect( (int)(SULX+695*1), (int)(0+240*1), 854, 480 ) , "- Reverse" , ScoreStyle); // Reverse
		GUI.Label( new Rect( (int)(SULX+695*1), (int)(0+294*1), 854, 480 ) , "- SpeedUp" , ScoreStyle); // SpeedUp
		GUI.Label( new Rect( (int)(SULX+695*1), (int)(0+346*1), 854, 480 ) , "- SlowDown" , ScoreStyle); // SlowDown

		//GUI.Label( new Rect( (int)(SULX+16), (int)(384), 64, 64 ) , "" , LeftArrowButton); // LeftArrowButton

		GUI.color = new Color(1.0f, 1.0f, 1.0f, 0.365f); // в начале задаём что сейчас будем рисовать непрозрачное, на всякиц случай

		GUI.Label( new Rect( (int)(SULX-3), (int)(0+3+4), 854, 480 ) , "SNAKE'84 v0.1 alpha" , GameTitleLabelStyle); // поцентру сверху , при условии что текст upper center
		GUI.Label( new Rect( (int)(SULX+32-3), (int)(0+37+4), 854, 480 ) , scoreString , ScoreStyle); // score
		GUI.Label( new Rect( (int)(SULX+649-3), (int)(0+37+4), 854, 480 ) , "Legend:" , ScoreStyle); // Legend:
		GUI.Label( new Rect( (int)(SULX+695-3), (int)(0+82+4), 854, 480 ) , "- Snake body" , ScoreStyle); // Snake body // всё очень точно и просто задаёться :)
		GUI.Label( new Rect( (int)(SULX+695-3), (int)(0+138+4), 854, 480 ) , "- Length +1" , ScoreStyle); // Length +1
		GUI.Label( new Rect( (int)(SULX+695-3), (int)(0+190+4), 854, 480 ) , "- Length -1" , ScoreStyle); // Length -1
		GUI.Label( new Rect( (int)(SULX+695-3), (int)(0+240+4), 854, 480 ) , "- Reverse" , ScoreStyle); // Reverse
		GUI.Label( new Rect( (int)(SULX+695-3), (int)(0+294+4), 854, 480 ) , "- SpeedUp" , ScoreStyle); // SpeedUp
		GUI.Label( new Rect( (int)(SULX+695-3), (int)(0+346+4), 854, 480 ) , "- SlowDown" , ScoreStyle); // SlowDown

		GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f); // в начале задаём что сейчас будем рисовать непрозрачное, на всякиц случай

		GUI.matrix = Matrix4x4.TRS(Vector3.zero,Quaternion.identity,new Vector3(1f,1f,1f)); // задаём нужный масштаб

		if (Screen.height <= 1024) for (int lx = 0; lx < (int)((Screen.height/32)+1); lx++) GUI.Box( new Rect( 0, 32*lx, Screen.width, 32 ) , "", ScreenLinesStyle); else // рисуем линий на весь экран
		for (int lx3 = 0; lx3 < (int)((Screen.height/(32*3))+1); lx3++) GUI.Box( new Rect( 0, 32*3*lx3, Screen.width, 32*3 ) , "", ScreenLinesStyleX3); // рисуем линий на весь экран

		GUI.Box( new Rect( 0, 0, Screen.width, Screen.height ) , "", ScreenGlossStyle); // рисуем картинку блика на весь экран

		arrowButtons[0].pixelInset = new Rect (16*GUIMultipler,32*GUIMultipler,64*GUIMultipler,64*GUIMultipler);
		arrowButtons[1].pixelInset = new Rect (144*GUIMultipler,32*GUIMultipler,64*GUIMultipler,64*GUIMultipler);
		arrowButtons[2].pixelInset = new Rect (80*GUIMultipler,80*GUIMultipler,64*GUIMultipler,64*GUIMultipler);
		arrowButtons[3].pixelInset = new Rect (80*GUIMultipler,16*GUIMultipler,64*GUIMultipler,64*GUIMultipler);
		
		arrowButtons[4].pixelInset = new Rect (13*GUIMultipler,25*GUIMultipler,64*GUIMultipler,64*GUIMultipler);
		arrowButtons[5].pixelInset = new Rect (141*GUIMultipler,25*GUIMultipler,64*GUIMultipler,64*GUIMultipler);
		arrowButtons[6].pixelInset = new Rect (77*GUIMultipler,73*GUIMultipler,64*GUIMultipler,64*GUIMultipler);
		arrowButtons[7].pixelInset = new Rect (77*GUIMultipler,9*GUIMultipler,64*GUIMultipler,64*GUIMultipler);
	}

	void Start ()
	{
		gameObject.GetComponent<AudioSource>().Play();
		// пройдёмся по массиву клеточек игрового поля , и подчистим всё , проинициализируем мир черных клеточке :)
		/*screenBlocks = new dummy2D[12];
		for (int cY = 0; cY < 12; cY++) { // проход по оси Y , тоесть по-строчно
			screenBlocks[cY] = new dummy2D();
			screenBlocks[cY].cellsX = new GameObject [12];
			for (int cX = 0; cX < 12; cX++) {
				screenBlocks[cY].cellsX[cX] = new GameObject();
			}
		}*/
		//теперь нам массив screenBlocks чистый и проинициализированый вдоль и поперёк :)

		//теперь создадим тело змейки из 4х кубиков
		for (int dx = 0; dx < 4; dx++) { // проход по оси Y , тоесть по-строчно
			snakeBodyBlocks[(5*12)+(dx+2)] = Instantiate (cellTypes[0], GameBoard.transform.position, GameBoard.transform.rotation) as GameObject;
			snakeBodyBlocks[(5*12)+(dx+2)].name = "cell_" + (dx + 2) + "_5";
			snakeBodyBlocks[(5*12)+(dx+2)].transform.parent = GameBoard;
			snakeBodyBlocks[(5*12)+(dx+2)].transform.localPosition = new Vector3(dx + 2, -5, 0); // создаём объект в мировых координатах на основе экранных координат курсора мышки
			snakeBodyBlocks[(5*12)+(dx+2)].transform.localScale = new Vector3 (1, 1, 1); 
			snakeBodyBlocks[(5*12)+(dx+2)].tag = "SnakeBody";
		}
		// укажем каждому кубику кто идёт за ним а кто перед ним
		snakeBodyBlocks[(5*12)+(0+2)].GetComponent<SnakeBodyBlockScript>().nextBodyPart = snakeBodyBlocks[(5*12)+(1+2)];
		snakeBodyBlocks[(5*12)+(1+2)].GetComponent<SnakeBodyBlockScript>().nextBodyPart = snakeBodyBlocks[(5*12)+(2+2)];
		snakeBodyBlocks[(5*12)+(2+2)].GetComponent<SnakeBodyBlockScript>().nextBodyPart = snakeBodyBlocks[(5*12)+(3+2)];

		snakeBodyBlocks[(5*12)+(3+2)].GetComponent<SnakeBodyBlockScript>().prevBodyPart = snakeBodyBlocks[(5*12)+(2+2)];
		snakeBodyBlocks[(5*12)+(2+2)].GetComponent<SnakeBodyBlockScript>().prevBodyPart = snakeBodyBlocks[(5*12)+(1+2)];
		snakeBodyBlocks[(5*12)+(1+2)].GetComponent<SnakeBodyBlockScript>().prevBodyPart = snakeBodyBlocks[(5*12)+(0+2)];
		// укажем где голова а где хвот
		firstSnakeCell = snakeBodyBlocks[(5*12)+(3+2)];
		lastSnakeCell = snakeBodyBlocks[(5*12)+(0+2)];

		score = 0;
		maxStepTimer = 0.7f;
		stepTimer = 0;
		firstCellPos = new Vector2(5,5);
		lastCellPos = new Vector2(2,5);
		wantedMoveDir = MoveDir.Right;
		moveDir = MoveDir.Right;
		canMove = true;
		gameStarted = true;
	}

	void Update ()
	{
		if (gameStarted)
		{
			for (int buttonNum = 0; buttonNum < arrowButtons.Length; buttonNum++)
			{
				if (Input.GetMouseButton (0) && arrowButtons [buttonNum] != null && arrowButtons [buttonNum].texture != null && arrowButtons [buttonNum].HitTest (Input.mousePosition))
				{
					//Debug.Log ("button : " + buttonNum);
					// теперь задаём новое направление движения согласно нажатой кнопке , толкьо с учётом текущего направления , что бы не убить себя начав идти внутрь тела... :)
					if (buttonNum == 0 /*Left*/ && moveDir != MoveDir.Right) {
						//Debug.Log ("Now move to Left");
						wantedMoveDir = MoveDir.Left;
					}
					if (buttonNum == 1 /*Right*/ && moveDir != MoveDir.Left) {
						//Debug.Log ("Now move to Right");
						wantedMoveDir = MoveDir.Right;
					}
					if (buttonNum == 2 /*Up*/ && moveDir != MoveDir.Down) {
						//Debug.Log ("Now move to Up");
						wantedMoveDir = MoveDir.Up;
					}
					if (buttonNum == 3 /*Down*/ && moveDir != MoveDir.Up) {
						//Debug.Log ("Now move to Down");
						wantedMoveDir = MoveDir.Down;
					}
				}
			}
			stepTimer += Time.deltaTime;
			if (stepTimer >= maxStepTimer)
			{
				moveDir = wantedMoveDir;
				UpdateScore();
				if (needFlipDir)
				{
					canMove = false;
					needFlipDir = false;
					ReverseMovement();
				}
				stepTimer = 0;
				//Debug.Log ("do step");
				// делаем шаг , принцып просто : мы переставляем последний кубик тела змейки на место перед её "головой" в сторону по направленю движения , и назначем этот кубик новой головй змейки
				if (canMove) {
					if (moveDir == MoveDir.Right) MakeAStep(1,0); 
					if (moveDir == MoveDir.Left) MakeAStep(-1,0);
					if (moveDir == MoveDir.Up) MakeAStep(0,-1);
					if (moveDir == MoveDir.Down) MakeAStep(0,1);
				}
				if (!canMove) canMove = true;

			}
		}
	}

	void MakeAStep(int dX, int dY)
	{
		newX = (int)(firstCellPos.x+dX);
		newY = (int)(firstCellPos.y+dY);
		if (newX >= 12) newX-=12;
		if (newX < 0) newX+=12;
		if (newY < 0) newY+=12;
		if (newY >= 12) newY-=12;

		dummyCell = snakeBodyBlocks [(int)((newY * 12) + newX)];
		if (dummyCell != null) { //0 - SnakeBody , 1 - PlusLength , 2 - MinusLength , 3 - Reverse , 4 - SpeedUp , 5 - SlowDown
			if (dummyCell.tag == "SnakeBody") StartCoroutine(GameOver()); else
			if (dummyCell.tag == "PlusLength") MakeAPlusLength(newX , newY); else
			if (dummyCell.tag == "MinusLength") MakeAMinusLength(newX , newY); else
			if (dummyCell.tag == "Reverse") MakeAReverse(newX , newY); else
			if (dummyCell.tag == "SpeedUp") ChangeGameSpeed(newX , newY , -0.1f); else
			if (dummyCell.tag == "SlowDown") ChangeGameSpeed(newX , newY , 0.1f);
		} else JustMoveForward ();
		//gameStarted = false;
	}

	void JustMoveForward ()
	{
		dummy2Cell = firstSnakeCell;
		firstSnakeCell.GetComponent<SnakeBodyBlockScript>().nextBodyPart = lastSnakeCell;
		dummyCell = lastSnakeCell.GetComponent<SnakeBodyBlockScript>().nextBodyPart;
		snakeBodyBlocks[(int)((lastCellPos.y * 12) + lastCellPos.x)] = null;
		lastSnakeCell.name = "cell_" + newX + "_" + newY;
		firstCellPos = new Vector2(newX ,  newY);
		lastSnakeCell.transform.localPosition = new Vector3 ( firstCellPos.x, -firstCellPos.y, 0);
		firstSnakeCell = lastSnakeCell;
		lastSnakeCell = dummyCell;
		lastCellPos = new Vector2((int)lastSnakeCell.transform.localPosition.x , -(int)lastSnakeCell.transform.localPosition.y);
		firstSnakeCell.GetComponent<SnakeBodyBlockScript>().nextBodyPart.GetComponent<SnakeBodyBlockScript>().prevBodyPart = null;
		firstSnakeCell.GetComponent<SnakeBodyBlockScript>().nextBodyPart = null;
		firstSnakeCell.GetComponent<SnakeBodyBlockScript>().prevBodyPart = dummy2Cell;
		snakeBodyBlocks[(int)((firstCellPos.y * 12) + firstCellPos.x)] = firstSnakeCell;
		appleStep += 1;
		if (appleStep >= 5)
		{
			appleStep = 0;
			RandomPlaceApple();
		}
	}

	void MakeAPlusLength (int needX , int needY)
	{
		currentApple = snakeBodyBlocks [(needY * 12) + needX];
		Destroy(currentApple);
		currentApple = Instantiate (cellTypes[0], GameBoard.transform.position, GameBoard.transform.rotation) as GameObject;
		currentApple.name = "cell_" + needX + "_" + needY;
		currentApple.transform.parent = GameBoard;
		currentApple.transform.localPosition = new Vector3(needX, -needY, 0); // создаём объект в мировых координатах на основе экранных координат курсора мышки
		currentApple.transform.localScale = new Vector3 (1, 1, 1); 
		snakeBodyBlocks[(needY*12)+needX] = currentApple;
		firstSnakeCell.GetComponent<SnakeBodyBlockScript>().nextBodyPart = currentApple;
		currentApple.GetComponent<SnakeBodyBlockScript>().prevBodyPart = firstSnakeCell;
		firstSnakeCell = currentApple;
		firstCellPos = new Vector2(needX , needY);
		snakeLength += 1;
		score += 2;
		stepTimer = 0;
		//JustMoveForward ();
	}

	void MakeAMinusLength(int needX , int needY)
	{
		currentApple = snakeBodyBlocks [(needY * 12) + needX];
		Destroy(currentApple);
		if (snakeLength >= 2) {
			dummyCell = lastSnakeCell.GetComponent<SnakeBodyBlockScript>().nextBodyPart;
			lastSnakeCell = null;
			Destroy(snakeBodyBlocks[(int)(lastCellPos.y*12)+(int)lastCellPos.x]);
			lastSnakeCell = dummyCell;
			dummyCell = null;
			lastCellPos = new Vector2((int)lastSnakeCell.transform.localPosition.x , -(int)lastSnakeCell.transform.localPosition.y);
			snakeLength -= 1;
			score += 1;
		} else score += 10;
		JustMoveForward ();
	}

	void MakeAReverse(int needX , int needY)
	{
		currentApple = snakeBodyBlocks [(needY * 12) + needX];
		Destroy(currentApple);
		needFlipDir = true;
		score += 1;
	}

	void ChangeGameSpeed(int needX , int needY , float dSpeed)
	{
		currentApple = snakeBodyBlocks [(needY * 12) + needX];
		Destroy(currentApple);
		maxStepTimer += dSpeed;
		if (maxStepTimer >= 0.7f) { maxStepTimer = 0.7f; score += 2; }
		if (maxStepTimer <= 0.1f) maxStepTimer = 0.1f;
		if (dSpeed < 0) score += 3; else score += 1;
		JustMoveForward ();
	}


	void ReverseMovement ()
	{
		gameStarted = false;
		//
		lastSnakeCell.SendMessage("MakeItReverse" , SendMessageOptions.DontRequireReceiver);

		dummyCell = lastSnakeCell;
		lastSnakeCell = firstSnakeCell;
		firstSnakeCell = dummyCell;

		dummyPos = lastCellPos;
		lastCellPos = firstCellPos;
		firstCellPos = dummyPos;
		//

		dummy2Cell = firstSnakeCell.GetComponent<SnakeBodyBlockScript>().prevBodyPart;
		if (dummy2Cell.transform.localPosition.x == firstSnakeCell.transform.localPosition.x && dummy2Cell.transform.localPosition.y >= firstSnakeCell.transform.localPosition.y) moveDir = MoveDir.Down;
		if (dummy2Cell.transform.localPosition.x == firstSnakeCell.transform.localPosition.x && dummy2Cell.transform.localPosition.y <= firstSnakeCell.transform.localPosition.y) moveDir = MoveDir.Up;

		if (dummy2Cell.transform.localPosition.y == firstSnakeCell.transform.localPosition.y && dummy2Cell.transform.localPosition.x >= firstSnakeCell.transform.localPosition.x) moveDir = MoveDir.Left;
		if (dummy2Cell.transform.localPosition.y == firstSnakeCell.transform.localPosition.y && dummy2Cell.transform.localPosition.x <= firstSnakeCell.transform.localPosition.x) moveDir = MoveDir.Right;
		//if (moveDir == MoveDir.Left) moveDir = MoveDir.Right; else
		//if (moveDir == MoveDir.Right) moveDir = MoveDir.Left; else
		//if (moveDir == MoveDir.Up) moveDir = MoveDir.Down; else
		//if (moveDir == MoveDir.Down) moveDir = MoveDir.Up;
		wantedMoveDir = moveDir;

		gameStarted = true;
	}

	IEnumerator GameOver()
	{
		Debug.Log("Game Over");
		gameStarted = false;
		gameObject.GetComponent<AudioSource>().Stop();

		stepTimer = -100000000;
		canMove = false;
		needFlipDir = false;

		dummyCell = null;
		dummy2Cell = null;
		firstSnakeCell = null;
		lastSnakeCell = null;

		yield return new WaitForSeconds (0.5f);

		for (int dx = 0; dx < 144; dx++) {
			if (snakeBodyBlocks[dx] != null) Destroy(snakeBodyBlocks[dx]);
		}

		yield return new WaitForSeconds (0.5f);

		Start ();
	}

	void RandomPlaceApple ()
	{

		int RandomAppleTipe = Random.Range(0,1000);
		//Debug.Log("RandomAppleTipe : " + RandomAppleTipe);
		//0 - SnakeBody , 1 - PlusLength , 2 - MinusLength , 3 - Reverse , 4 - SpeedUp , 5 - SlowDown
		if (RandomAppleTipe >= 500) PlaceAppleOnBoard(1); else
		if (RandomAppleTipe >= 450) PlaceAppleOnBoard(2); else
		if (RandomAppleTipe >= 400) PlaceAppleOnBoard(3); else
		if (RandomAppleTipe >= 300) PlaceAppleOnBoard(4); else
		if (RandomAppleTipe >= 250) PlaceAppleOnBoard(5);// else
		//if (RandomAppleTipe >= 500) PlaceAppleOnBoard(1);
	}

	void PlaceAppleOnBoard(int appleType) // 0 - SnakeBody , 1 - PlusLength , 2 - MinusLength , 3 - Reverse , 4 - SpeedUp , 5 - SlowDown
	{
		//int RandomAppleCoordinate = Random.Range(0,143);
		int appleX = Random.Range(0,11);
		int appleY = Random.Range(0,11);
		Debug.Log("appleType : " + appleType + "  appleX: " + appleX + "  appleY: " + appleY);
		currentApple = snakeBodyBlocks[(appleY*12)+appleX];
		if (currentApple == null)
		{
			currentApple = Instantiate (cellTypes[appleType], GameBoard.transform.position, GameBoard.transform.rotation) as GameObject;
			currentApple.name = "cell_" + appleX + "_" + appleY;
			currentApple.transform.parent = GameBoard;
			currentApple.transform.localPosition = new Vector3(appleX, -appleY, 0); // создаём объект в мировых координатах на основе экранных координат курсора мышки
			currentApple.transform.localScale = new Vector3 (1, 1, 1); 
			snakeBodyBlocks[(appleY*12)+appleX] = currentApple;//snakeBodyBlocks[RandomAppleCoordinate].tag = "SnakeBody";
			currentApple = null;
		}
	}

	void UpdateScore()
	{
		if (score <= 9) scoreString = "Score: " + "0000000" + score; else
		if (score <= 99) scoreString = "Score: " + "000000" + score; else
		if (score <= 999) scoreString = "Score: " + "00000" + score; else
		if (score <= 9999) scoreString = "Score: " + "0000" + score; else
		if (score <= 99999) scoreString = "Score: " + "000" + score; else
		if (score <= 999999) scoreString = "Score: " + "00" + score; else
		if (score <= 9999999) scoreString = "Score: " + "0" + score; else
		if (score <= 99999999) scoreString = "Score: " + "" + score;
		//scoreLabel.text = "Score: " + scoreString;
		//scoreLabelShadow.text = "Score: " + scoreString;
	}


}
