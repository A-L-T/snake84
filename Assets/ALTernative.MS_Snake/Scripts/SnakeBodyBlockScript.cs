using UnityEngine;
using System.Collections;

public class SnakeBodyBlockScript : MonoBehaviour {

	public GameObject nextBodyPart = null;

	public GameObject prevBodyPart = null;

	private GameObject dummy = null;

	void MakeItReverse()
	{
		dummy = null;
		if (nextBodyPart != null && prevBodyPart == null) // вариант 1 - последняя точка
		{
			nextBodyPart.SendMessage("MakeItReverse" , SendMessageOptions.DontRequireReceiver);
			prevBodyPart = nextBodyPart;
			nextBodyPart = null;
		} else
		if (nextBodyPart != null && prevBodyPart != null) // вариант 2 - точка в середине
		{
			nextBodyPart.SendMessage("MakeItReverse" , SendMessageOptions.DontRequireReceiver);
			dummy = nextBodyPart;
			nextBodyPart = prevBodyPart;
			prevBodyPart = dummy;
		} else
		if (nextBodyPart == null && prevBodyPart != null) // вариант 3 - первая точка
		{
			nextBodyPart = prevBodyPart;
			prevBodyPart = null;
		}
	}

}
